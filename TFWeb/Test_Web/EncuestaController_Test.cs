﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TFWeb.Controllers;
using System.Web.Mvc;

namespace Test_Web
{
    [TestClass]
    public class EncuestaController_Test
    {
        [TestMethod]
        public void IndexTest()
        {
            //Arrange
            EncuestaController encuesta = new EncuestaController();
            //Act
            ViewResult result = encuesta.Index() as ViewResult;
            //Assert
            Assert.IsNotNull(result);
            
        }
        [TestMethod]
        public void Vista_UsuarioTest()
        {
            //Arrange
            EncuestaController encuesta = new EncuestaController();
            //Act
            ViewResult result = encuesta.Vista_Usuario() as ViewResult;
            //Assert
            Assert.IsNotNull(result);

        }
        [TestMethod]
        public void Vista_DirectorTest()
        {
            //Arrange
            EncuestaController encuesta = new EncuestaController();
            //Act
            ViewResult result = encuesta.Vista_Director() as ViewResult;
            //Assert
            Assert.IsNotNull(result);

        }
    }
}
