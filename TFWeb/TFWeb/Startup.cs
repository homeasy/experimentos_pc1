﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TFWeb.Startup))]
namespace TFWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
