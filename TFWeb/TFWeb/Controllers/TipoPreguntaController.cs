﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TFWeb.Models;

namespace TFWeb.Controllers
{
    public class TipoPreguntaController : Controller
    {
        private TrabajoFinalEntities db = new TrabajoFinalEntities();

        // GET: /TipoPregunta/
        public ActionResult Index()
        {
            return View(db.TipoPregunta.ToList());
        }

        // GET: /TipoPregunta/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoPregunta tipopregunta = db.TipoPregunta.Find(id);
            if (tipopregunta == null)
            {
                return HttpNotFound();
            }
            return View(tipopregunta);
        }

        // GET: /TipoPregunta/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /TipoPregunta/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id_tipoPreg,nombreTipoPreg")] TipoPregunta tipopregunta)
        {
            if (ModelState.IsValid)
            {
                db.TipoPregunta.Add(tipopregunta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipopregunta);
        }

        // GET: /TipoPregunta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoPregunta tipopregunta = db.TipoPregunta.Find(id);
            if (tipopregunta == null)
            {
                return HttpNotFound();
            }
            return View(tipopregunta);
        }

        // POST: /TipoPregunta/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id_tipoPreg,nombreTipoPreg")] TipoPregunta tipopregunta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipopregunta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipopregunta);
        }

        // GET: /TipoPregunta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoPregunta tipopregunta = db.TipoPregunta.Find(id);
            if (tipopregunta == null)
            {
                return HttpNotFound();
            }
            return View(tipopregunta);
        }

        // POST: /TipoPregunta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoPregunta tipopregunta = db.TipoPregunta.Find(id);
            db.TipoPregunta.Remove(tipopregunta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
