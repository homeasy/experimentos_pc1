﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TFWeb.Models;

namespace TFWeb.Controllers
{
    public class PreguntaEncuestaController : Controller
    {
        private TrabajoFinalEntities db = new TrabajoFinalEntities();

        // GET: /PreguntaEncuesta/
        public ActionResult Index()
        {
            var preguntaencuesta = db.PreguntaEncuesta.Include(p => p.TipoPregunta).Include(p => p.Encuesta);
            return View(preguntaencuesta.ToList());
        }

        // GET: /PreguntaEncuesta/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PreguntaEncuesta preguntaencuesta = db.PreguntaEncuesta.Find(id);
            if (preguntaencuesta == null)
            {
                return HttpNotFound();
            }
            return View(preguntaencuesta);
        }

        // GET: /PreguntaEncuesta/Create
        public ActionResult Create()
        {
            ViewBag.id_tipoPreg = new SelectList(db.TipoPregunta, "id_tipoPreg", "nombreTipoPreg");
            ViewBag.id_encuesta = new SelectList(db.Encuesta, "id_encuesta", "nombre");
            return View();
        }

        // POST: /PreguntaEncuesta/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id_pregEncuesta,id_encuesta,id_tipoPreg,descripcion,orden")] PreguntaEncuesta preguntaencuesta)
        {
            if (ModelState.IsValid)
            {
                db.PreguntaEncuesta.Add(preguntaencuesta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_tipoPreg = new SelectList(db.TipoPregunta, "id_tipoPreg", "nombreTipoPreg", preguntaencuesta.id_tipoPreg);
            ViewBag.id_encuesta = new SelectList(db.Encuesta, "id_encuesta", "nombre", preguntaencuesta.id_encuesta);
            return View(preguntaencuesta);
        }

        // GET: /PreguntaEncuesta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PreguntaEncuesta preguntaencuesta = db.PreguntaEncuesta.Find(id);
            if (preguntaencuesta == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_tipoPreg = new SelectList(db.TipoPregunta, "id_tipoPreg", "nombreTipoPreg", preguntaencuesta.id_tipoPreg);
            ViewBag.id_encuesta = new SelectList(db.Encuesta, "id_encuesta", "nombre", preguntaencuesta.id_encuesta);
            return View(preguntaencuesta);
        }

        // POST: /PreguntaEncuesta/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id_pregEncuesta,id_encuesta,id_tipoPreg,descripcion,orden")] PreguntaEncuesta preguntaencuesta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(preguntaencuesta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_tipoPreg = new SelectList(db.TipoPregunta, "id_tipoPreg", "nombreTipoPreg", preguntaencuesta.id_tipoPreg);
            ViewBag.id_encuesta = new SelectList(db.Encuesta, "id_encuesta", "nombre", preguntaencuesta.id_encuesta);
            return View(preguntaencuesta);
        }

        // GET: /PreguntaEncuesta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PreguntaEncuesta preguntaencuesta = db.PreguntaEncuesta.Find(id);
            if (preguntaencuesta == null)
            {
                return HttpNotFound();
            }
            return View(preguntaencuesta);
        }

        // POST: /PreguntaEncuesta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PreguntaEncuesta preguntaencuesta = db.PreguntaEncuesta.Find(id);
            db.PreguntaEncuesta.Remove(preguntaencuesta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
