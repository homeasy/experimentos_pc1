﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TFWeb.Models;

namespace TFWeb.Controllers
{
    public class UsuarioController : Controller
    {
        private TrabajoFinalEntities db = new TrabajoFinalEntities();

        // GET: /Usuario/
        public ActionResult Index()
        {
            var usuario = db.Usuario.Include(u => u.Rol);
            return View(usuario.ToList());
        }

        // GET: /Usuario/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // GET: /Usuario/Create
        public ActionResult Create()
        {
            ViewBag.id_rol = new SelectList(db.Rol, "id_rol", "name_Rol");
            return View();
        }

        // POST: /Usuario/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id_user,nombre,password,id_rol,fecha_nac,username,email")] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Usuario.Add(usuario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_rol = new SelectList(db.Rol, "id_rol", "name_Rol", usuario.id_rol);
            return View(usuario);
        }

        // GET: /Usuario/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_rol = new SelectList(db.Rol, "id_rol", "name_Rol", usuario.id_rol);
            return View(usuario);
        }

        // POST: /Usuario/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id_user,nombre,password,id_rol,fecha_nac,username,email")] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_rol = new SelectList(db.Rol, "id_rol", "name_Rol", usuario.id_rol);
            return View(usuario);
        }

        // REGISTRO
        public ActionResult Registro()
        {
            var id_rol = new SelectList(db.Rol, "id_rol", "name_Rol");
            return View();
        }

        // LOGIN
        public ActionResult Login()
        {
            return View();
        }

        // POST: /Usuario/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "username,password")] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                //  db.Users.Add(user);
                // db.SaveChanges();
                var query = from u in db.Usuario.Include("Rol")
                            where u.username == usuario.username
                            select u;
                Usuario objUser = query.FirstOrDefault();
                if (objUser != null)
                {
                    if (objUser.password == usuario.password)
                    {
                        Session["CurrentUser"] = objUser;
                        int idUs = objUser.id_user;
                        switch (objUser.Rol.name_Rol)
                        {
                            case "Encuestado":
                                return RedirectToAction("Vista_Usuario", "Encuesta");
                                break;
                            case "Admin":
                                return RedirectToAction("Index", "Encuesta");
                                break;
                            case "Director":
                                return RedirectToAction("Vista_Director", "Encuesta");
                                break;
                        }
                        ViewBag.LoginMessage = "Tipo de usuario invalido!";
                    }
                    else
                    {
                        ViewBag.LoginMessage = "Contraseña incorrecta!";
                    }
                }
                else
                {
                    ViewBag.LoginMessage = "No existe este usuario!";
                }
            }
            return View(usuario);
        }

        // GET: /Usuario/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuario.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: /Usuario/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Usuario usuario = db.Usuario.Find(id);
            db.Usuario.Remove(usuario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
