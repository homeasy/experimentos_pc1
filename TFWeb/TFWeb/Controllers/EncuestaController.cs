﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TFWeb.Models;

namespace TFWeb.Controllers
{
    public class EncuestaController : Controller
    {
        private TrabajoFinalEntities db = new TrabajoFinalEntities();

        // GET: /Encuesta/
        public ActionResult Index()
        {
            return View(db.Encuesta.ToList());
        }

        // GET: /Encuesta/---VISTA USUARIO------------------------------------------------
        public ActionResult Vista_Usuario()
        {
            return View(db.Encuesta.ToList());
        }
        // GET: /Encuesta/---VISTA DIRECTOR---------------------------------------------------------
        public ActionResult Vista_Director()
        {
            return View(db.Encuesta.ToList());
        }

        // GET: /Encuesta/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Encuesta encuesta = db.Encuesta.Find(id);
            if (encuesta == null)
            {
                return HttpNotFound();
            }
            return View(encuesta);
        }
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // GET: /Encuesta/Details/5
        public ActionResult Detalle_Usuario(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Encuesta encuesta = db.Encuesta.Find(id);
            if (encuesta == null)
            {
                return HttpNotFound();
            }
            return View(encuesta);
        }
        // GET: /Encuesta/Details/5
        public ActionResult Detalle_Director(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Encuesta encuesta = db.Encuesta.Find(id);
            if (encuesta == null)
            {
                return HttpNotFound();
            }
            return View(encuesta);
        }
        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        // GET: /Encuesta/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Encuesta/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id_encuesta,nombre,descripcion,fechaTentativa,indicador,estado,costo")] Encuesta encuesta)
        {
            if (ModelState.IsValid)
            {
                db.Encuesta.Add(encuesta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(encuesta);
        }

        //-------------------------------------------------------------------------------------------------------------
       public ActionResult ListaPreguntas(int id)
        {
            var query = from e in db.Encuesta.Include(e => e.nombre)
                        join p in db.PreguntaEncuesta on e.id_encuesta equals p.id_encuesta
                        where p.id_encuesta == id
                        select p;
            List<PreguntaEncuesta> preguntas = query.ToList();
            return View(preguntas);
        }

       [HttpPost]
       [ValidateAntiForgeryToken]
       public ActionResult ListaPreguntas()
       {
           int eid = Convert.ToInt32(Request.Form["encuesta_id"]);
           Usuario usuario = (Usuario)Session["CurrentUser"];
           int idUs = usuario.id_user;
           var query = from p in db.PreguntaEncuesta
                       where p.id_encuesta == eid
                       select p;
           List<PreguntaEncuesta> preguntas = (List<PreguntaEncuesta>)query.ToList();
           if (preguntas.Count>0)
           {
               List<Respuesta> rs = new List<Respuesta>();
               for (int i = 0; i < preguntas.Count; i++)
               {
                   Respuesta r = new Respuesta();
                   //Si concatenas el string con numero convierte el numero a string con ToString()
                   r.respuestaTexto = Request.Form.Get("rpta+" + preguntas.ElementAt(i).orden.ToString());
                   r.id_pregunta = preguntas.ElementAt(i).id_pregEncuesta;
                   r.id_usuario = idUs;
                   rs.Add(r);
               }
               for (int i = 0; i < rs.Count;i++)
               {
                   db.Respuesta.Add(rs.ElementAt(i));
                   //Antes el metodo db.SaveChanges(); estaba afuera del for pero debe ir adentro, ya que vas a guardar pregunta por 
                   //pregunta 
                   db.SaveChanges();
               }               
           }
           //Despues de guardar retorna el metodo ActionResult de este controlador
           return RedirectToAction("Vista_Usuario");
       } 
        //-------------------------------------------------------------------------------------------------------------
       public ActionResult ListaEncuestas()
       {
           var query = (from r in db.Respuesta
                       join p in db.PreguntaEncuesta on r.id_pregunta equals p.id_pregEncuesta
                       join e in db.Encuesta on p.id_encuesta equals e.id_encuesta
                       select new { e.nombre, r.id_usuario }).ToList();
           ViewBag.detalle = query;
           return View();
       }
        //-------------------------------------------------------------------------------------------------------------

        // GET: /Encuesta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Encuesta encuesta = db.Encuesta.Find(id);
            if (encuesta == null)
            {
                return HttpNotFound();
            }
            return View(encuesta);
        }

        // POST: /Encuesta/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id_encuesta,nombre,descripcion,fechaTentativa,indicador,estado,costo")] Encuesta encuesta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(encuesta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(encuesta);
        }

        // GET: /Encuesta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Encuesta encuesta = db.Encuesta.Find(id);
            if (encuesta == null)
            {
                return HttpNotFound();
            }
            return View(encuesta);
        }

        // POST: /Encuesta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Encuesta encuesta = db.Encuesta.Find(id);
            db.Encuesta.Remove(encuesta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
