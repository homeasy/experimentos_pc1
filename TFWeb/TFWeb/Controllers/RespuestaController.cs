﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TFWeb.Models;

namespace TFWeb.Controllers
{
    public class RespuestaController : Controller
    {
        private TrabajoFinalEntities db = new TrabajoFinalEntities();

        // GET: /Respuesta/
        public ActionResult Index()
        {
            var respuesta = db.Respuesta.Include(r => r.PreguntaEncuesta).Include(r => r.Usuario);
            return View(respuesta.ToList());
        }

        // GET: /Respuesta/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Respuesta respuesta = db.Respuesta.Find(id);
            if (respuesta == null)
            {
                return HttpNotFound();
            }
            return View(respuesta);
        }

        // GET: /Respuesta/Create
        public ActionResult Create()
        {
            ViewBag.id_pregunta = new SelectList(db.PreguntaEncuesta, "id_pregEncuesta", "descripcion");
            ViewBag.id_usuario = new SelectList(db.Usuario, "id_user", "nombre");
            return View();
        }

        // POST: /Respuesta/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id_respuesta,id_pregunta,id_usuario,respuestaTexto,respuestaNumerico,respuestaBit")] Respuesta respuesta)
        {
            if (ModelState.IsValid)
            {
                db.Respuesta.Add(respuesta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_pregunta = new SelectList(db.PreguntaEncuesta, "id_pregEncuesta", "descripcion", respuesta.id_pregunta);
            ViewBag.id_usuario = new SelectList(db.Usuario, "id_user", "nombre", respuesta.id_usuario);
            return View(respuesta);
        }

        // GET: /Respuesta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Respuesta respuesta = db.Respuesta.Find(id);
            if (respuesta == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_pregunta = new SelectList(db.PreguntaEncuesta, "id_pregEncuesta", "descripcion", respuesta.id_pregunta);
            ViewBag.id_usuario = new SelectList(db.Usuario, "id_user", "nombre", respuesta.id_usuario);
            return View(respuesta);
        }

        // POST: /Respuesta/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id_respuesta,id_pregunta,id_usuario,respuestaTexto,respuestaNumerico,respuestaBit")] Respuesta respuesta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(respuesta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_pregunta = new SelectList(db.PreguntaEncuesta, "id_pregEncuesta", "descripcion", respuesta.id_pregunta);
            ViewBag.id_usuario = new SelectList(db.Usuario, "id_user", "nombre", respuesta.id_usuario);
            return View(respuesta);
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        public ActionResult PregRespuesta()
        {
            ViewBag.id_pregunta = new SelectList(db.PreguntaEncuesta, "id_pregEncuesta", "descripcion");
            ViewBag.id_usuario = new SelectList(db.Usuario, "id_user", "nombre");
            return View();
        }

        // POST: /Respuesta/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PregRespuesta([Bind(Include = "id_respuesta,id_pregunta,id_usuario,respuestaTexto,respuestaNumerico,respuestaBit")] Respuesta respuesta)
        {
            if (ModelState.IsValid)
            {
                db.Respuesta.Add(respuesta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_pregunta = new SelectList(db.PreguntaEncuesta, "id_pregEncuesta", "descripcion", respuesta.id_pregunta);
            ViewBag.id_usuario = new SelectList(db.Usuario, "id_user", "nombre", respuesta.id_usuario);
            return View(respuesta);
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        // GET: /Respuesta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Respuesta respuesta = db.Respuesta.Find(id);
            if (respuesta == null)
            {
                return HttpNotFound();
            }
            return View(respuesta);
        }

        // POST: /Respuesta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Respuesta respuesta = db.Respuesta.Find(id);
            db.Respuesta.Remove(respuesta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
