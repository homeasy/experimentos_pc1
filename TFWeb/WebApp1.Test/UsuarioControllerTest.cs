﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TFWeb.Controllers;
using TFWeb.Models;

namespace WebApp1.Test
{
    [TestClass]
    public class UsuarioControllerTest
    {
        [TestMethod]
        public void Index()
        {
            UsuarioController controller = new UsuarioController();

            ViewResult resultado = controller.Index() as ViewResult;
            Assert.IsNotNull(resultado);
        }

        [TestMethod]
        public void Detalle()
        {
            UsuarioController controller = new UsuarioController();
            Usuario usuario = new Usuario { id_user = 2 };

            var resultado = controller.Details(usuario);
            Assert.IsNotNull(resultado.ViewBag.MensajeError);
        }
        [TestMethod]
        public void Crear()
        {
            UsuarioController controller = new UsuarioController();
            Usuario usuario = new Usuario { nombre = "daniela", email = "dani@gmail.com" };

            var resultado = controller.Create(usuario);
            Assert.IsNotNull(resultado.ViewBag.MensajeError);
        }
        [TestMethod]
        public void Editar()
        {
            UsuarioController controller = new UsuarioController();
            Usuario usuario = new Usuario { id_user = 1 };

            var resultado = controller.Edit(usuario);
            Assert.IsNotNull(resultado.ViewBag.MensajeError);
        }
        [TestMethod]
        public void Registro()
        {
            UsuarioController controller = new UsuarioController();

            ViewResult resultado = controller.Index() as ViewResult;
            Assert.IsNotNull(resultado);
        }
        [TestMethod]
        public void Login()
        {
            UsuarioController controller = new UsuarioController();
            Usuario usuario = new Usuario { };

            ViewResult resultado = controller.Login() as ViewResult;
            Assert.IsNotNull(resultado);
        }
        [TestMethod]
        public void Eliminar()
        {
            UsuarioController controller = new UsuarioController();
            Usuario usuario = new Usuario { id_user = 2 };

            var resultado = controller.Delete(usuario);
            Assert.IsTrue(resultado.ViewBag.Message);
        }
    }
}
